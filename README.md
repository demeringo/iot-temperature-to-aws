# iot-temperature-to-aws
Measure temperature with a raspberry pi and aggregate the values in a AWS dynamodb.

## Principle
1. The raspberry sends data in JSON to AWS IOT gateway. Data is transmitted through MQTT protocol by using AWS IOT SDK.
2. IOT gateway uses a rule to filter received messages and propagate data to dynamodb.


![Archi overview](picts/Temperature_to_iot_dynamo.png)

## Usage
### Configuration on aws
Create all aws resources:

```
cd aws-setup
./aws-resources-setup.sh
```

TODO: fix hardcoded table arn in policy document

Setup details are explained in the jupyter notebook: [aws-setup/iot-temperature-to-aws.ipynb](aws-setup/iot-temperature-to-aws.ipynb)

*Note: This code was written before I became familiar with Terraform, today I would recommend using terraform for creating the insfrastructure instead of plain AWS CLI commands like above*

### Configuration on the raspberry
TODO: complete this section
- install iot sdk on the raspberry
- install custom_code to publish to the specific MQTT topic.

## References
- https://github.com/adhorn/rasp-sensehat-iot
- https://github.com/chiradeep/idle-instance-reaper?files=1
