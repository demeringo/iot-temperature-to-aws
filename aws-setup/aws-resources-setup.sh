#!/bin/bash
set -x

TABLE_NAME=iot_datazz
ROLE_NAME=iot_sensor_rolezz
POLICY_NAME=iot_table_write_policyzz
THING_NAME=lab-temp-sensorzz
MQTT_TOPIC=lab/temperaturezz
RULE_NAME=temperature_to_dynamo_z

table_arn=$(aws dynamodb create-table --table-name=$TABLE_NAME \
                                      --cli-input-json file://iot_data_table_def.json \
                                      --output text \
                                      --query 'TableDescription.TableArn')


role_arn=$(aws iam create-role --role-name=$ROLE_NAME \
                               --assume-role-policy-document=file://iot_sensor_role.json \
                               --output text \
                               --query 'Role.Arn')

sleep 3
policy_arn=$(aws iam create-policy --policy-name=$POLICY_NAME \
                                   --policy-document=file://iot_table_write_policy.json \
                                   --output text \
                                   --query 'Policy.Arn')

sleep 3
aws iam attach-role-policy --role-name=$ROLE_NAME --policy-arn=$policy_arn

sleep 3
aws iot create-thing --thing-name=$THING_NAME

sleep 4
aws iot create-topic-rule --rule-name $RULE_NAME --topic-rule-payload='{
  "sql": "SELECT * FROM '$MQTT_TOPIC'",
  "ruleDisabled": false,
  "awsIotSqlVersion": "2016-03-23",
  "actions": [{
      "dynamoDB": {
          "tableName": '\"$TABLE_NAME\"',
          "roleArn": '\"$role_arn\"',
          "hashKeyField": "topic",
          "hashKeyValue": "${topic(2)}",
          "rangeKeyField": "timestamp",
          "rangeKeyValue": "${timestamp()}"
      }
  }]
}'
